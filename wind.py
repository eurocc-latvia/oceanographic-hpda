import numpy as np
import datetime
import pandas as pd
import matplotlib.pyplot as plt
#import matplotlib.colors as colors

nmod=7
name=np.array(['modLiep{}'.format(i+1) for i in range(nmod)])
cl=['black','blue','yellow','magenta','lime','brown','orange','cyan']
dfB = {}
dfE = {}
dfL = {}
dfV = {}
for i in range(nmod):
    dfB[i] = 'dfB'+str(i)
    dfE[i] = 'dfE'+str(i)
    dfL[i] = 'dfL'+str(i)
    dfV[i] = 'dfV'+str(i)

for i in range(nmod):
    dfB[i] = pd.DataFrame(columns=["u10m","v10m", "prec", "time"]) 
    dfE[i] = pd.DataFrame(columns=["u10m","v10m", "prec", "time"])
    dfL[i] = pd.DataFrame(columns=["u10m","v10m", "prec", "time"])
    dfV[i] = pd.DataFrame(columns=["u10m","v10m", "prec", "time"])
    ti=''
    icnt=0
    with open('../'+name[i]+'/grib2cmod.log') as f:
        for line in f:
            if (ti=='' and line[0:].startswith(' forecast hour=')):
                ti=line[44:]
                tim=ti.split()
                tim1=datetime.datetime(hour=np.int(tim[0]),day=np.int(tim[2]),month=np.int(tim[4]),year=np.int(tim[6])) 
                #print(tim1)
            if line[0:].startswith(' u10m:'):
                u10m=line[33:58]
            if line[0:].startswith(' v10m:'):
                v10m=line[33:58]
            if (line[0:].startswith(' prec') and icnt==3):
                dfV[i]=dfV[i].append(pd.DataFrame([[float(u10m),float(v10m),float(line[33:58]),tim1]], columns=dfV[i].columns),ignore_index=True)
                icnt=0
            if (line[0:].startswith(' prec') and icnt==2):
                dfL[i]=dfL[i].append(pd.DataFrame([[float(u10m),float(v10m),float(line[33:58]),tim1]], columns=dfL[i].columns),ignore_index=True)
                icnt=3
            if (line[0:].startswith(' prec') and icnt==1):
                dfE[i]=dfE[i].append(pd.DataFrame([[float(u10m),float(v10m),float(line[33:58]),tim1]], columns=dfE[i].columns),ignore_index=True)
                icnt=2
            if (line[0:].startswith(' prec') and ti!=''):
                dfB[i]=dfB[i].append(pd.DataFrame([[float(u10m),float(v10m),float(line[33:58]),tim1]], columns=dfB[i].columns),ignore_index=True)
                ti=''
                icnt=1
    dfB[i].index=dfB[i]['time']
    dfE[i].index=dfE[i]['time']
    dfL[i].index=dfL[i]['time']
    dfV[i].index=dfV[i]['time']
    #print(dfB[i])
    #print(dfE[i])
    #print(dfL[i])
    #print(dfV[i])


fig, ax = plt.subplots(figsize=(20,12))
for i in range(nmod):
    dfB[i]['mod']=np.hypot(dfB[i]['u10m'],dfB[i]['v10m'])
    ax.quiver(dfB[i].index,dfB[i]['mod'],dfB[i]['u10m'],dfB[i]['v10m'],scale=400,label=name[i],color=cl[i])
plt.grid()
ax.legend(loc='best')
plt.xlabel('Time UTC')
plt.ylabel('wind speed [m/s]')
plt.title('model winds in Baltic proper')
plt.savefig('pics/windBalticProper.png')
plt.close()

fig, ax = plt.subplots(figsize=(20,12))
for i in range(nmod):
    dfE[i]['mod']=np.hypot(dfE[i]['u10m'],dfE[i]['v10m'])
    ax.quiver(dfE[i].index,dfE[i]['mod'],dfE[i]['u10m'],dfE[i]['v10m'],scale=400,label=name[i],color=cl[i])
plt.grid()
ax.legend(loc='best')
plt.xlabel('Time UTC')
plt.ylabel('wind speed [m/s]')
plt.title('model winds in Liepaja lake')
plt.savefig('pics/windLiepajaLake.png')
plt.close()


fig, ax = plt.subplots(figsize=(20,12))
for i in range(nmod):
    dfL[i]['mod']=np.hypot(dfL[i]['u10m'],dfL[i]['v10m'])
    ax.quiver(dfL[i].index,dfL[i]['mod'],dfL[i]['u10m'],dfL[i]['v10m'],scale=400,label=name[i],color=cl[i])
plt.grid()
ax.legend(loc='best')
plt.xlabel('Time UTC')
plt.ylabel('wind speed [m/s]')
plt.title('model winds in Liepaja port')
plt.savefig('pics/windLiepajaPort.png')
plt.close()


fig, ax = plt.subplots(figsize=(20,12))
for i in range(nmod):
    dfV[i]['mod']=np.hypot(dfV[i]['u10m'],dfV[i]['v10m'])
    ax.quiver(dfV[i].index,dfV[i]['mod'],dfV[i]['u10m'],dfV[i]['v10m'],scale=400,label=name[i],color=cl[i])
plt.grid()
ax.legend(loc='best')
plt.xlabel('Time UTC')
plt.ylabel('wind speed [m/s]')
plt.title('model winds in Ventspils port')
plt.savefig('pics/windVentspilsPort.png')
plt.close()


