import xarray as xr
#import cartopy
#import cartopy.feature as cfeature
import numpy as np
import datetime
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import shapefile
from unidecode import unidecode

wlstat=pd.read_fwf('modLiep/wlstat.dat',colspecs=[(9,13),(17,33),(34,38),(38,43),(43,48),(49,53),(54,60)],skiprows=[0,1,2,3,4,5,6,7,9])  
wlstat=wlstat.astype({"I": np.int, "J": np.int,"GRID": np.int, "FILE": np.int,"NUMBER": np.int})
wlfil=wlstat.loc[wlstat['NAME'] == 'Liepaja'].FILE
wlfi=wlfil.values.tolist()[0]
print(wlfi)
wlfil=wlstat.loc[wlstat['FILE'] == wlfi].NAME
wlfin=wlfil.values.tolist()
wlfinc=['date','time','dtime','step']+wlfin
print(wlstat)
print(wlfin)

df= pd.read_csv('modLiep/wlfile'+np.str(wlfi),delim_whitespace=True, parse_dates=True,index_col=2,header=None,names = wlfinc)
#df.index=pd.to_datetime(df.index, unit='D',origin=datetime.datetime(1900,1,1))
df=df[df['time'].str[0:2]!='24']
df=df.reset_index()
df['dt']=[datetime.datetime.strptime(df.date[i]+' '+df.time[i], "%Y.%m.%d %H:%M:%S") for i in range(len(df["date"]))]
df.index=df.dt
print(df)  

fig, ax = plt.subplots(figsize=(20,12))
for st in wlfin:
    print(st)
    ax.plot(df.index,df[st],label=st)
plt.grid()
ax.legend(loc='best')
plt.xlabel('Time UTC')
plt.ylabel('wat. lev. [mm]')
plt.title('water level')
plt.savefig('pics/watlev.png')
plt.close()

shp = shapefile.Reader('LV/linLV.shp')

shpr = shapefile.Reader('LV/upes.shp')
fields = shpr.fields[1:] 
field_names = [field[0] for field in fields] 
mainriv=['Venta','Barta','Alande','Bartuva','Tosele','Kazupe']

shpcr = shapefile.Reader('LV/strauti.shp')
fieldscr = shpcr.fields[1:] 
field_namescr = [field[0] for field in fieldscr] 
maincr=['Otanka']

shpl = shapefile.Reader('LV/ezeri.shp')
fieldsl = shpl.fields[1:] 
field_namesl = [field[0] for field in fieldsl] 
mainlak=['Liepajas e','Durbes eze','Tasu ezers','Papes ezer']

def OSM(ax):
    for shape in shp.shapeRecords():
        x = [i[0] for i in shape.shape.points[:]]
        y = [i[1] for i in shape.shape.points[:]]
        plt.plot(x,y,color='black',zorder=-3)
    atrpr=''
    ariv=['fake']
    for shape in shpr.shapeRecords():
        x = [i[0] for i in shape.shape.points[:]]
        y = [i[1] for i in shape.shape.points[:]]
        ax.plot(x,y,color='cyan',linewidth=1,zorder=-4)
        atr = dict(zip(field_names, shape.record)) 
        if unidecode(atr['name'][:10]) in mainriv:
            ax.plot(x,y,color='blue',linewidth=1,zorder=-4)
            if ((atr['name']!=atrpr) and (atr['name'] not in ariv)):
                #print(atr['name'])
                ax.annotate(atr['name'], (x[len(x) // 2],y[len(y) // 2]),zorder=-4)
                atrpr=atr['name']
                ariv.append(atrpr)
    atrpr=''
    ariv=['fake']        
    for shape in shpl.shapeRecords():
        x = [i[0] for i in shape.shape.points[:]]
        y = [i[1] for i in shape.shape.points[:]]
        ax.plot(x,y,color='lightgrey',linewidth=1,zorder=-5)
        atr = dict(zip(field_namesl, shape.record)) 
        if unidecode(atr['name'][:10]) in mainlak:
            ax.plot(x,y,color='grey',linewidth=1,zorder=-5)
            if ((atr['name']!=atrpr) and (atr['name'] not in ariv)):
                #print(atr['name'])
                ax.annotate(atr['name'], (x[len(x) // 2],y[len(y) // 2]),zorder=-5)
                atrpr=atr['name']
                ariv.append(atrpr)
    atrpr=''
    ariv=['fake']        
    for shape in shpcr.shapeRecords():
        x = [i[0] for i in shape.shape.points[:]]
        y = [i[1] for i in shape.shape.points[:]]
        ax.plot(x,y,color='cyan',linewidth=0.8,zorder=-6)
        atr = dict(zip(field_namescr, shape.record)) 
        if unidecode(atr['name'][:10]) in maincr:
            ax.plot(x,y,color='blue',linewidth=0.8,zorder=-6)
            if ((atr['name']!=atrpr) and (atr['name'] not in ariv)):
                #print(atr['name'])
                ax.annotate(atr['name'], (x[len(x) // 2],y[len(y) // 2]),zorder=-6)
                atrpr=atr['name']
                ariv.append(atrpr)

ds_Liep = xr.open_mfdataset("modLiep/Liepaja20*.nc")  #,engine='netcdf4'
print(ds_Liep)

ds_LiepE = xr.open_mfdataset("modLiep/LiepajaE20*.nc")
print(ds_LiepE)

ds_Vent = xr.open_mfdataset("modLiep/Venstpils20*.nc")
print(ds_Vent)

for iii in range(ds_Liep.sizes['time']):
    fig = plt.figure(figsize=(24,15))
    ax = fig.add_subplot(121)
    #ax = plt.axes()
    T=ds_Liep.elev[iii]
    T1=ds_LiepE.elev[iii]
    vmin=min(T1.min(skipna=True).values,T1.min(skipna=True).values)
    vmax=max(T1.max(skipna=True).values,T1.max(skipna=True).values)
    print('vmin=',vmin,' vmax=',vmax)
    c=T.plot(ax=ax,cmap=plt.cm.jet,vmin=vmin,vmax=vmax)
    c=T1.plot(ax=ax,cmap=plt.cm.jet,vmin=vmin,vmax=vmax,add_colorbar=False)
    for index, st in wlstat.iterrows():
        if st.GRID==3:
            ax.scatter(T['lon'][st['J']-1],T['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax.annotate(st['NAME'], (T['lon'][st['J']-1],T['lat'][st['I']-1]),fontsize ='x-large')
        if st.GRID==2:
            ax.scatter(T1['lon'][st['J']-1],T1['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax.annotate(st['NAME'], (T1['lon'][st['J']-1],T1['lat'][st['I']-1]),fontsize ='x-large')
    #plt.title('Surface salinity 2015-01-09T15:00:00')
    OSM(ax)
    ax.set_xlim(20.92,21.15)
    ax.set_ylim(56.395,56.575)
    ax2 = fig.add_subplot(122)
    T=ds_Vent.elev[iii]
    #vmin=T.min(skipna=True).values
    vmax=T.max(skipna=True).values
    print('vmin=',vmin,' vmax=',vmax)
    c=T.plot(ax=ax2,cmap=plt.cm.jet,vmin=vmin,vmax=vmax)
    for index, st in wlstat.iterrows():
        if st.GRID==4:
            ax2.scatter(T['lon'][st['J']-1],T['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax2.annotate(st['NAME'], (T['lon'][st['J']-1],T['lat'][st['I']-1]),fontsize ='x-large')
    #plt.title('Surface salinity 2015-01-09T15:00:00')
    OSM(ax2)
    ax2.set_xlim(21.51,21.61)
    ax2.set_ylim(57.36,57.43)
    plt.savefig('pics/wl_'+str(iii)+'.png')
    plt.close()

    fig = plt.figure(figsize=(24,15))
    ax = fig.add_subplot(121)
    #ax = plt.axes()
    T=ds_Liep.salt[iii,0]
    T1=ds_LiepE.salt[iii,0]
    vmin=min(T.min(skipna=True).values,T1.min(skipna=True).values)
    vmax=max(T.max(skipna=True).values,T1.max(skipna=True).values)
    print('vmin=',vmin,' vmax=',vmax)
    c=T.plot(ax=ax,cmap=plt.cm.jet,vmin=vmin,vmax=vmax)
    c=T1.plot(ax=ax,cmap=plt.cm.jet,vmin=vmin,vmax=vmax,add_colorbar=False)
    for index, st in wlstat.iterrows():
        if st.GRID==3:
            ax.scatter(T['lon'][st['J']-1],T['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax.annotate(st['NAME'], (T['lon'][st['J']-1],T['lat'][st['I']-1]),fontsize ='x-large')
        if st.GRID==2:
            ax.scatter(T1['lon'][st['J']-1],T1['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax.annotate(st['NAME'], (T1['lon'][st['J']-1],T1['lat'][st['I']-1]),fontsize ='x-large')
    #plt.title('Surface salinity 2015-01-09T15:00:00')
    OSM(ax)
    ax.set_xlim(20.92,21.15)
    ax.set_ylim(56.395,56.575)
    ax2 = fig.add_subplot(122)
    T=ds_Vent.salt[iii,0]
    vmin=T.min(skipna=True).values
    vmax=T.max(skipna=True).values
    print('vmin=',vmin,' vmax=',vmax)
    c=T.plot(ax=ax2,cmap=plt.cm.jet,vmin=vmin,vmax=vmax)
    for index, st in wlstat.iterrows():
        if st.GRID==4:
            ax2.scatter(T['lon'][st['J']-1],T['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax2.annotate(st['NAME'], (T['lon'][st['J']-1],T['lat'][st['I']-1]),fontsize ='x-large')
    #plt.title('Surface salinity 2015-01-09T15:00:00')
    OSM(ax2)
    ax2.set_xlim(21.51,21.61)
    ax2.set_ylim(57.36,57.43)
    plt.savefig('pics/sal_'+str(iii)+'.png')
    plt.close()

    fig = plt.figure(figsize=(24,15))
    ax = fig.add_subplot(121)
    #ax = plt.axes()
    T=ds_Liep.temp[iii,0]
    T1=ds_LiepE.temp[iii,0]
    vmin=min(T.min(skipna=True).values,T1.min(skipna=True).values)
    vmax=max(T.max(skipna=True).values,T1.max(skipna=True).values)
    print('vmin=',vmin,' vmax=',vmax)
    c=T.plot(ax=ax,cmap=plt.cm.jet,vmin=vmin,vmax=vmax)
    c=T1.plot(ax=ax,cmap=plt.cm.jet,vmin=vmin,vmax=vmax,add_colorbar=False)
    for index, st in wlstat.iterrows():
        if st.GRID==3:
            ax.scatter(T['lon'][st['J']-1],T['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax.annotate(st['NAME'], (T['lon'][st['J']-1],T['lat'][st['I']-1]),fontsize ='x-large')
        if st.GRID==2:
            ax.scatter(T1['lon'][st['J']-1],T1['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax.annotate(st['NAME'], (T1['lon'][st['J']-1],T1['lat'][st['I']-1]),fontsize ='x-large')
    #plt.title('Surface salinity 2015-01-09T15:00:00')
    OSM(ax)
    ax.set_xlim(20.92,21.15)
    ax.set_ylim(56.395,56.575)
    ax2 = fig.add_subplot(122)
    T=ds_Vent.temp[iii,0]
    vmin=T.min(skipna=True).values
    vmax=T.max(skipna=True).values
    print('vmin=',vmin,' vmax=',vmax)
    c=T.plot(ax=ax2,cmap=plt.cm.jet,vmin=vmin,vmax=vmax)
    for index, st in wlstat.iterrows():
        if st.GRID==4:
            ax2.scatter(T['lon'][st['J']-1],T['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax2.annotate(st['NAME'], (T['lon'][st['J']-1],T['lat'][st['I']-1]),fontsize ='x-large')
    #plt.title('Surface salinity 2015-01-09T15:00:00')
    OSM(ax2)
    ax2.set_xlim(21.51,21.61)
    ax2.set_ylim(57.36,57.43)
    plt.savefig('pics/tmp_'+str(iii)+'.png')
    plt.close()

    fig = plt.figure(figsize=(24,15))
    ax = fig.add_subplot(121)
    po=14
    U=ds_Liep["uvel"][iii,0]
    V=ds_Liep["vvel"][iii,0]
    Upo=ds_Liep["uvel"].isel(lat=slice(None,None,po), lon=slice(None,None,po),time=iii,depth=0)
    Vpo=ds_Liep["vvel"].isel(lat=slice(None,None,po), lon=slice(None,None,po),time=iii,depth=0)
    Upo=Upo / np.power(Upo**2 + Vpo**2+1e-9,0.25)
    Vpo=Vpo / np.power(Upo**2 + Vpo**2+1e-9,0.25)
    M = np.hypot(U, V)
    Mpo = np.hypot(Upo, Vpo)
    U1=ds_LiepE["uvel"][iii,0]
    V1=ds_LiepE["vvel"][iii,0]
    po=7
    Upo1=ds_LiepE["uvel"].isel(lat=slice(None,None,po), lon=slice(None,None,po),time=iii,depth=0)
    Vpo1=ds_LiepE["vvel"].isel(lat=slice(None,None,po), lon=slice(None,None,po),time=iii,depth=0)
    Upo1=Upo1 / np.power(Upo1**2 + Vpo1**2+1e-9,0.25)
    Vpo1=Vpo1 / np.power(Upo1**2 + Vpo1**2+1e-9,0.25)
    M1 = np.hypot(U1, V1)
    Mpo1 = np.hypot(Upo1, Vpo1)
    vmax=max(M.max(skipna=True).values,M1.max(skipna=True).values)
    M.plot(ax=ax,cmap=plt.cm.jet,vmax=vmax,norm=colors.PowerNorm(gamma=0.5))
    M1.plot(ax=ax,cmap=plt.cm.jet,vmax=vmax,add_colorbar=False,norm=colors.PowerNorm(gamma=0.5))
    ax.quiver(Upo.lon,Upo.lat,Upo,Vpo,Mpo,scale = 9,cmap=plt.cm.Greys)
    ax.quiver(Upo1.lon,Upo1.lat,Upo1,Vpo1,Mpo1,scale = 9,cmap=plt.cm.Greys)
    print('vmin=',vmin,' vmax=',vmax)
    for index, st in wlstat.iterrows():
        if st.GRID==3:
            ax.scatter(U['lon'][st['J']-1],U['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax.annotate(st['NAME'], (U['lon'][st['J']-1],U['lat'][st['I']-1]),fontsize ='x-large')
        if st.GRID==2:
            ax.scatter(U1['lon'][st['J']-1],U1['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax.annotate(st['NAME'], (U1['lon'][st['J']-1],U1['lat'][st['I']-1]),fontsize ='x-large')
    #plt.title('Surface salinity 2015-01-09T15:00:00')
    OSM(ax)
    ax.set_xlim(20.92,21.15)
    ax.set_ylim(56.395,56.575)
    ax2 = fig.add_subplot(122)
    po=7
    U=ds_Vent["uvel"][iii,0]
    V=ds_Vent["vvel"][iii,0]
    Upo=ds_Vent["uvel"].isel(lat=slice(None,None,po), lon=slice(None,None,po),time=iii,depth=0)
    Vpo=ds_Vent["vvel"].isel(lat=slice(None,None,po), lon=slice(None,None,po),time=iii,depth=0)
    Upo=Upo / np.power(Upo**2 + Vpo**2+1e-9,0.25)
    Vpo=Vpo / np.power(Upo**2 + Vpo**2+1e-9,0.25)
    M = np.hypot(U, V)
    Mpo = np.hypot(Upo, Vpo)
    vmax=M.max(skipna=True).values
    M.plot(ax=ax2,cmap=plt.cm.jet,vmax=vmax,norm=colors.PowerNorm(gamma=0.5))
    ax2.quiver(Upo.lon,Upo.lat,Upo,Vpo,Mpo,scale = 9,cmap=plt.cm.Greys)
    print('vmin=',vmin,' vmax=',vmax)
    for index, st in wlstat.iterrows():
        if st.GRID==4:
            ax2.scatter(U['lon'][st['J']-1],U['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax2.annotate(st['NAME'], (U['lon'][st['J']-1],U['lat'][st['I']-1]),fontsize ='x-large')
    #plt.title('Surface salinity 2015-01-09T15:00:00')
    OSM(ax2)
    ax2.set_xlim(21.51,21.61)
    ax2.set_ylim(57.36,57.43)
    plt.savefig('pics/cur_'+str(iii)+'.png')
    plt.close()

    fig = plt.figure(figsize=(24,15))
    ax = fig.add_subplot(121)
    po=14
    U=ds_Liep["uwind"][iii]
    V=ds_Liep["vwind"][iii]
    Upo=ds_Liep["uwind"].isel(lat=slice(None,None,po), lon=slice(None,None,po),time=iii)
    Vpo=ds_Liep["vwind"].isel(lat=slice(None,None,po), lon=slice(None,None,po),time=iii)
    Upo=Upo / np.power(Upo**2 + Vpo**2+1e-9,0.25)
    Vpo=Vpo / np.power(Upo**2 + Vpo**2+1e-9,0.25)
    M = np.hypot(U, V)
    Mpo = np.hypot(Upo, Vpo)
    U1=ds_LiepE["uwind"][iii]
    V1=ds_LiepE["vwind"][iii]
    po=7
    Upo1=ds_LiepE["uwind"].isel(lat=slice(None,None,po), lon=slice(None,None,po),time=iii)
    Vpo1=ds_LiepE["vwind"].isel(lat=slice(None,None,po), lon=slice(None,None,po),time=iii)
    Upo1=Upo1 / np.power(Upo1**2 + Vpo1**2+1e-9,0.25)
    Vpo1=Vpo1 / np.power(Upo1**2 + Vpo1**2+1e-9,0.25)
    M1 = np.hypot(U1, V1)
    Mpo1 = np.hypot(Upo1, Vpo1)
    vmax=max(M.max(skipna=True).values,M1.max(skipna=True).values)
    M.plot(ax=ax,cmap=plt.cm.jet,vmax=vmax,norm=colors.PowerNorm(gamma=0.5))
    M1.plot(ax=ax,cmap=plt.cm.jet,vmax=vmax,add_colorbar=False,norm=colors.PowerNorm(gamma=0.5))
    ax.quiver(Upo.lon,Upo.lat,Upo,Vpo,Mpo,scale = 100,cmap=plt.cm.Greys)
    ax.quiver(Upo1.lon,Upo1.lat,Upo1,Vpo1,Mpo1,scale = 100,cmap=plt.cm.Greys)
    print('vmin=',vmin,' vmax=',vmax)
    for index, st in wlstat.iterrows():
        if st.GRID==3:
            ax.scatter(U['lon'][st['J']-1],U['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax.annotate(st['NAME'], (U['lon'][st['J']-1],U['lat'][st['I']-1]),fontsize ='x-large')
        if st.GRID==2:
            ax.scatter(U1['lon'][st['J']-1],U1['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax.annotate(st['NAME'], (U1['lon'][st['J']-1],U1['lat'][st['I']-1]),fontsize ='x-large')
    #plt.title('Surface salinity 2015-01-09T15:00:00')
    OSM(ax)
    ax.set_xlim(20.92,21.15)
    ax.set_ylim(56.395,56.575)
    ax2 = fig.add_subplot(122)
    po=7
    U=ds_Vent["uwind"][iii]
    V=ds_Vent["vwind"][iii]
    Upo=ds_Vent["uwind"].isel(lat=slice(None,None,po), lon=slice(None,None,po),time=iii)
    Vpo=ds_Vent["vwind"].isel(lat=slice(None,None,po), lon=slice(None,None,po),time=iii)
    Upo=Upo / np.power(Upo**2 + Vpo**2+1e-9,0.25)
    Vpo=Vpo / np.power(Upo**2 + Vpo**2+1e-9,0.25)
    M = np.hypot(U, V)
    Mpo = np.hypot(Upo, Vpo)
    vmax=M.max(skipna=True).values
    M.plot(ax=ax2,cmap=plt.cm.jet,vmax=vmax,norm=colors.PowerNorm(gamma=0.5))
    ax2.quiver(Upo.lon,Upo.lat,Upo,Vpo,Mpo,scale = 100,cmap=plt.cm.Greys)
    print('vmin=',vmin,' vmax=',vmax)
    for index, st in wlstat.iterrows():
        if st.GRID==4:
            ax2.scatter(U['lon'][st['J']-1],U['lat'][st['I']-1], edgecolors='black',marker='o',s=60,facecolors='none')
            ax2.annotate(st['NAME'], (U['lon'][st['J']-1],U['lat'][st['I']-1]),fontsize ='x-large')
    #plt.title('Surface salinity 2015-01-09T15:00:00')
    OSM(ax2)
    ax2.set_xlim(21.51,21.61)
    ax2.set_ylim(57.36,57.43)
    plt.savefig('pics/wnd_'+str(iii)+'.png')
    plt.close()


