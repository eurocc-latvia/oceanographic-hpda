#!/bin/bash

#cur_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )  #if bash
#op_dir=$cur_dir                                            #if bash

if [ -n $SLURM_JOB_ID ] ; then
THEPATH=$(scontrol show job $SLURM_JOBID | awk -F= '/Command=/{print $2}')
else
THEPATH=$(realpath $0)
fi
cur_dir=$(dirname $(dirname "${THEPATH}"))
op_dir=$cur_dir/work

#SBATCH --job-name=Liepaja
#SBATCH --partition=debug #regular
#SBATCH -c 1
#SBATCH --nodes=1
#SBATCH --time=UNLIMITED

echo $op_dir
cd $op_dir
export LD_LIBRARY_PATH=bin/local/lib
echo "starting tempdat2nc"
tempdat2nc/tempdat2nc
echo "finish"
