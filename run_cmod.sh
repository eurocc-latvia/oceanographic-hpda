#!/bin/bash
#SBATCH --job-name=Liepaja
#SBATCH --partition=regular
#SBATCH -c 16
#SBATCH --nodes=1
#SBATCH --time=UNLIMITED

#cur_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )  # if bash job
#op_dir=$cur_dir/modLiep                                    # if bash job

if [ -n $SLURM_JOB_ID ] ; then
THEPATH=$(scontrol show job $SLURM_JOBID | awk -F= '/Command=/{print $2}')
else
THEPATH=$(realpath $0)
fi
cur_dir=$(dirname $(dirname "${THEPATH}"))
op_dir=$cur_dir/work/modLiep
bin_dir=$cur_dir/work/bin

if [ -n "$SLURM_CPUS_PER_TASK" ]; then
  cores=$SLURM_CPUS_PER_TASK
else
  cores=1
fi
echo Using $cores cores

echo $op_dir
echo $bin_dir
cd $op_dir
echo "starting cmod"
export OMP_NUM_THREADS=$cores
$bin_dir/cmod
echo "finish"
