# Oceanographic HPDA

HPC demo-case for the purposes of the project "National Competence Centres in the framework of EuroHPC" (EuroCC)

The demo-case is designed for operational modelling of coastal estuaries at western coast of Latvia, i.e., of Liepaja-port, Liepaja lake and Ventspils port taking into account interaction with the Baltic proper. High performance computing is essential for this task. The model solves physical hydrodynamics within coastal areas: 3-dimensional currents, salinity and temperature and surface elevation, ice concentration and thickness. Atmospheric forcing is provided by forecasts of DMI Harmonie model with 2.5 km resolution. Outer boundary conditions are derived from Copernicus Marine Service (CMEMS). Hydrological inflows are estimed from latest observations by LEGMC, but users can specify their own inflows, inflow temperature or inflow salinity. Initial condition corresponds to operational model run in HywasPort service.

https://www.frontiersin.org/articles/10.3389/fmars.2021.657721/full

http://www.water.lv/HywasPort/

# Basic structure

Unzipped tar folder contains the model setups, i.e., folders that are generated in last 7 days "modLiep1", "modLiep1",..., "modLiep7", where the higher number means older setup. Each of the 5 day long setup is generated from model runs of the operational coastal service Hywasport approximately at noon of every day. Each of the folder complete information for the model run of 5 days. User can freely modify these setups to see their own needs.
The user works in "work" folder. It contains main scripts that will perform the tasks:

1. wind.py   - for initial inspection of wind variation in each domain within the forecast period,

![windLiepajaPort](img/windLiepajaPort.png "wind Liepaja port")

2. pre_process_modify.sh   - for selecting and modifying the setup for demo run,

3. run_cmod.sh   - for executing the demo case,

4. run_postprocess.sh   - for converting model outputs to NetCDF files,

5. post.py   - for creating of overview PNG files from NetCDF files.

"work" directory contains several subfolders:

1. bin   - there the executables of ocean circulation model are stored,

2. pic   - there the overview PNG pictures are created,

![cur_18](img/cur_18.png "example currents")
![sal_18](img/sal_18.png "example salinity")
![watlev](img/watlev.png "example water level")

3. tempdat2nc   - there the utility for conversion from model files to NetCDF files is stored,

4. modLiep   - there the current model setup is stored,

5. LV   - there the ShapeFiles of geospatial layers are stored that are used for generating PNG files.

# Requirements

Linux system with at least 9 GB of free space and multicore processor and OMP paralellization.
GNU Fortran compiler gfortran
Python with packages numpy, datetime, matplotlib, cartopy, xarray, netcdf4, PyShp, pandas, unidecode

# Quick start

Create and enter into a empty folder with arbitrary name in Linux based High Performance Computer with at least 9 GB of free storage capacity and Python installed:
~~~
$ mkdir testLiep
$ cd testLiep
~~~
Required python packages: numpy, matplotlib, datetime, pandas, PyShp, xarray, ncdef4, unidecode
Download the latest oceanographic setup by:
~~~
$ wget www.modlab.lv/RigasOsta/modeLiepu.tar.gz
~~~
Untar the setup by:
~~~
$ tar -xvf modeLiepu.tar.gz
~~~
Change directory to work directory of current oceanographic setup:
~~~
$ cd work
~~~
If the user does not have an own Python distribution (python3 command does not work) it can be loaded in HPC with
~~~
$ module load anaconda3/anaconda-2020.11
~~~
Run
~~~
$ python3 wind.py
~~~
to create overview of wind forecasts for past 7 days. The created average wind forecasts of each domain are in "pic" folder.
Edit constants in "run_preprocess_modify.sh":

1. select a starting day (1-7), where 1 is most recent and 7 is a 7 days old setup,

2. modify run-off [m³/s] of rivers,

3. modify river temperature,

4. modify river salinity.

Run the preprocesser:
~~~
$ bash run_preprocess_modify.sh
~~~
Run the oceanographic circulation model:
~~~
$ sbatch -c a run_cmod.sh
~~~
where "a" is 1-16 is number of cores that are going to be used and available in the system. It could take couple of days for the model to finish, but you can run next post-processing commands as soon as necessary. 
When running next commands, ensure that you are still working in ".../work" directory, in case if you have closed the terminal during longer run of ocean circulation model. You can check current directory by:
~~~
$ pwd
~~~
Check the execution time:
~~~
$ cat modLiep/zeitschritt
~~~
Run postprocessing utility after at least one hour is passed in the circulation model:
~~~
$ sbatch run_postprocess.sh
~~~
Now, the *.nc files are created in modLiep directory that could be viewed by any software in https://www.unidata.ucar.edu/software/netcdf/software.html. Tool "ncview" is recomended.

Alternatively, one can use the python to create overview pictures.
Python code requires several packages: matplotlib, xarray, datetime, netcdf4, PyShp, dask. These packages can be loaded in a separate Anaconda environment by:
~~~
$ conda create -n cmod
$ source activate cmod
$ conda install -c conda-forge numpy pandas xarray netCDF4 cartopy unidecode pyshp dask
~~~
With python configured, the overview pictures are created by (remove or modify lines 16-17 if using your own python environment in "run_python_post.py")
~~~
$ sbatch run_python_post.py
~~~
The overview pictures will appear in "pic" folder for every hour in circulation model. Sea level is update frequency is 10 minutes in the model for stations selected in modLiep/wlstat.dat file. All the other quantities are stored as hourly instantaneous values.
The postprocessing tools can be repeated as many times as necessary.

![watlev](img/watlev.png "example water level")

To kill the excution of oceanographic circulation model use "scancel" command. Otherwise, it will continue untill 5 days of model run are completed.

# Advanced configuration

Advanced users can enter "work/modLiep/" folder and modify setup according to their own needs. It contains a number of text files which can be modified and they influence the results of the demo run.
The meaning of some important files in setup "work/modLiep" directory:

1. metast_neu   - There a period of simulation can be changed. Changing from letter "V" to "Q" will switch to run without meteorological forcing,

2. options.nml - There several options and parameters could be touched: include/exclude dynamic ice, include/exclude precipitation evaporation, include/exclude tidal potential, etc.

3. data files for each domain - Change, e.g., time step (time steps for each domain should be consistent),

4. cmod.nml - change update frequncy of variables and output frequency of results,

5. wlstat.dat - include additional locations where 10 minute water level variation is included.

Be careful with editing these data files (except NML files), the parameters should be placed at *exact* row and column.
