#!/bin/bash
#SBATCH --job-name=PyPostLiep
#SBATCH --partition=regular  #Rindas nosaukums
#SBATCH --ntasks=1     # Kodolu/procesoru skaits
#SBATCH --time=06:00:00 # Laikalimits,h:min:s

if [ -n $SLURM_JOB_ID ] ; then
THEPATH=$(scontrol show job $SLURM_JOBID | awk -F= '/Command=/{print $2}')
else
THEPATH=$(realpath $0)
fi
cur_dir=$(dirname $(dirname "${THEPATH}"))
echo $cur_dir/work
cd $cur_dir/work

module load anaconda3/anaconda-2020.11
source activate cmod
python3 post.py