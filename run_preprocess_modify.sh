#!/bin/bash
cur_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
op_dir=$cur_dir/modLiep

#clean up work directory
cd $op_dir
rm -f *

#copying daysback=1-7 days old setup
#specify daysback:
daysback=1
cp ../../modLiep$daysback/* .

#----------------modifying setup-------------------------------
#---modifying river inflow---
#modify river inflow files riverinflow_0? for each hour in work directory or specify a constant runoff (m3/s) for each river
#to leave the river runoff untouched set runoff_river=-1
runoff_Barta=15.0
runoff_Alande=2.0
runoff_Otanke=2.0
runoff_Venta=80.0

mv riverinflow_02 riverinflow_02_
head -n 1 riverinflow_02_ > riverinflow_02
awk -v var1="$runoff_Barta" -v var2="$runoff_Otanke" 'NR!=1 {printf("%s%8.1f%8.1f\n",substr($0,1,19),var1,var2) }' riverinflow_02_ >> riverinflow_02
mv riverinflow_03 riverinflow_03_
head -n 1 riverinflow_03_ > riverinflow_03
awk -v var1="$runoff_Alande" 'NR!=1 {printf("%s%8.1f\n",substr($0,1,19),var1) }' riverinflow_03_ >> riverinflow_03
mv riverinflow_04 riverinflow_04_
head -n 1 riverinflow_04_ > riverinflow_04
awk -v var1="$runoff_Venta" 'NR!=1 {printf("%s%8.1f\n",substr($0,1,19),var1) }' riverinflow_04_ >> riverinflow_04

#---modify river temperature---
#modify river files riverTS_0? for each hour in work directory or specify a constant temperture for each river
tmp_Barta=5.0
tmp_Otanke=4.0
tmp_Alande=3.0
tmp_Venta=2.0
#---modify river salinity---
#modify river files riverTS_0? for each hour in work directory or specify a constant salinity for each river
sal_Barta=1.5
sal_Otanke=1.4
sal_Alande=1.3
sal_Venta=1.2

mv riverTS_02 riverTS_02_
head -n 1 riverTS_02_ > riverTS_02
awk -v var1="$tmp_Barta" -v var2="$tmp_Otanke" -v var3="$sal_Barta" -v var4="$sal_Otanke" '(NR!=1 && NR % 2 == 0) {printf("%s%8.1f%8.1f\n",substr($0,1,19),var1,var2) } (NR!=1 && NR % 2 == 1) {printf("%s%8.1f%8.1f\n",substr($0,1,19),var3,var4) }' riverTS_02_ >> riverTS_02
mv riverTS_03 riverTS_03_
head -n 1 riverTS_03_ > riverTS_03
awk -v var1="$tmp_Alande" -v var3="$sal_Alande" '(NR!=1 && NR % 2 == 0) {printf("%s%8.1f\n",substr($0,1,19),var1) } (NR!=1 && NR % 2 == 1) {printf("%s%8.1f\n",substr($0,1,19),var3) }' riverTS_03_ >> riverTS_03
mv riverTS_04 riverTS_04_
head -n 1 riverTS_04_ > riverTS_04
awk -v var1="$tmp_Venta" -v var3="$sal_Venta" '(NR!=1 && NR % 2 == 0) {printf("%s%8.1f\n",substr($0,1,19),var1) } (NR!=1 && NR % 2 == 1) {printf("%s%8.1f\n",substr($0,1,19),var3) }' riverTS_04_ >> riverTS_04
